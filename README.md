![App icon](https://bytebucket.org/luka-zitnik/scrum-poker/raw/2.1/icon.svg)

# Scrum Poker

> A cheerful deck of Scrum poker cards.

Actually, a few popular decks to choose from, and a "+" button to add cards to the chosen deck if you need more. Check "Flip cards" and shake your phone to reveal the front side of each chosen card or uncheck it to show the front side only. Optional "Communication cards".

## Build Commands

Pack for testing on an Android device with ``ant apk``.

Pack for Marketplace with ``ant zip``.

## Release History

Release Date | Release Version |  Description
------------------|----------------------|----------------
2015-02-15 | 2.1 | Animates cards' shadow to and back from fully opaque.
2014-10-05 | 2.0 | Sharpens back illustration on Firefox OS 1.1, adds two new back illustrations, adds cancel button to the settings view.
2014-09-24 | 1.2 | Won't allow creation of blank cards.
2014-09-21 | 1.1 | Dims everything behind a selected card when "Flip cards" is unchecked, same as when "Flip cards" is checked.
2014-09-14 | 1.0