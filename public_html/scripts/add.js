(function() {
    var openViewButton = document.getElementById("toolbar-add"),
        view = document.getElementById("add-view"),
        addForm = view.querySelector("form"),
        doneButton = document.getElementById("add-done"),
        resetButton = addForm.querySelector("button[type=reset]"),
        closeButton = document.getElementById("add-exit"),
        touchstart = "ontouchstart" in window ? "touchstart" : "mousedown";

    function checkDisableDoneButton() {

        for (var i in this.elements) {
            if (this.elements.hasOwnProperty(i) && !this.elements[i].validity.valid) {
                doneButton.disabled = true;
                return;
            }
        }

        doneButton.disabled = false;
    }

    openViewButton.addEventListener("click", function() {
        var response;

        if (poker.deckSize() >= 30) {
            response = window.confirm("Only 30 cards are allowed in a deck. Would you like to remove all the cards you added?");

            if (response === true) {
                poker.restart();
            }
            else {
                return;
            }
        }

        checkDisableDoneButton.call(addForm);

        view.setAttribute("aria-hidden", "false");
    });

    resetButton.addEventListener(touchstart, function(event) {
        event.target.previousElementSibling.value = "";
        checkDisableDoneButton.call(addForm);
    });

    addForm.addEventListener("input", checkDisableDoneButton);

    closeButton.addEventListener("click", function() {
        view.setAttribute("aria-hidden", "true");
    });

    doneButton.addEventListener("click", function() {
        var input = view.querySelector("input"),
            value = input.value;

        poker.addCardToDeck(value);

        view.setAttribute("aria-hidden", "true");
        input.value = "";
    });
}());