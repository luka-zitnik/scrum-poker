(function() {

    function fillInFormFromConfig() {
        document.getElementById("settings-deck").value = config.deck;
        document.getElementById("settings-flip").checked = config.flip;
        document.getElementById("settings-image").value = config.image;
        document.getElementById("settings-communication-cards").checked = config.communicationCards;
    }

    function saveToConfig() {
        config.deck = document.getElementById("settings-deck").value;
        config.flip = document.getElementById("settings-flip").checked;
        config.image = document.getElementById("settings-image").value;
        config.communicationCards = document.getElementById("settings-communication-cards").checked;
    }

    fillInFormFromConfig();

    document.getElementById("toolbar-settings").addEventListener("click", function() {
        document.getElementById("settings-view").setAttribute("aria-hidden", "false");
    });

    document.getElementById("settings-exit").addEventListener("click", function() {
        document.getElementById("settings-view").setAttribute("aria-hidden", "true");
        fillInFormFromConfig();
    });

    document.getElementById("settings-done").addEventListener("click", function() {
        saveToConfig();
        poker.restart();
        document.getElementById("settings-view").setAttribute("aria-hidden", "true");
    });

})();