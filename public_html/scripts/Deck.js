var Deck = function(values, table, cardType, back) {

    this.cards = [];

    for (var i = 0; i < values.length; ++i) {
        this.cards[i] = new cardType(values[i], table, back);
    }

    this.add = function(value) {
        var newCard = new cardType(value, table, back);
        this.cards.push(newCard);
        return newCard;
    };

};