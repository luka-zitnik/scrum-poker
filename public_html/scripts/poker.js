var poker = {

    app: document.querySelector("[role='application']"),
    main: null,
    toolbar: null,
    deck: null,

    setup: function() {
        var that = this;

        this.main = this.app.querySelector("[role='main']");
        this.toolbar = this.app.querySelector("[role='toolbar']");
        this.start();

        overlay.create();

        window.addEventListener("pokerstart", function() {
            that.main.style.setProperty("overflow", "hidden");
            window.setTimeout(function() {
                that.toolbar.setAttribute("aria-hidden", "true");
                overlay.show();
            }, 0); // Don't interfere with overflow setting for smoother transition
        });

        window.addEventListener("pokerend", function() {
            window.setTimeout(function() {
                that.main.style.removeProperty("overflow");
            }, 600); // Do this only after the 0.5sec animation has finished
            that.toolbar.setAttribute("aria-hidden", "false");
            overlay.hide();
        });
    },

    start: function() {
        this.createDeck();
        this.layOut();
    },

    stop: function() {
        this.destroyDeck();
    },

    restart: function() {
        this.stop();
        this.start();
    },

    createDeck: function() {
        var sequences = {
                fibonacci: ["1", "2", "3", "5", "8", "13", "21", "34", "55", "89", "144"],
                modifiedFibonacci: ["0.5", "1", "2", "3", "5", "8", "13", "20", "40", "100"],
                tShirt: ["xs", "s", "m", "l", "xl", "xxl"],
                standard: ["A", "2", "3", "5", "8", "K"],
                communication: ["?", "∞", "☕", "0"]
            },
            cardBack,
            cardType,
            cardSequence;

        if (config.flip) {
            cardType = Card;
        }
        else {
            cardType = CardWithoutFlipAnimation;
        }

        switch (config.deck) {
            case "Modified Fibonacci":
                cardSequence = sequences.modifiedFibonacci;
                break;
            case "T-shirt":
                cardSequence = sequences.tShirt;
                break;
            case "Playing cards":
                cardSequence = sequences.standard;
                break;
            case "Fibonacci":
            default:
                cardSequence = sequences.fibonacci;
                break;
        }

        if (config.communicationCards) {
            cardSequence = sequences.communication.concat(cardSequence);
        }

        switch (config.image) {
            case "King Limpid":
                cardBack = new Back("images/King_Limpid.svg", "king-limpid");
                break;
            case "King Solid":
                cardBack = new Back("images/King_Solid.svg", "king-solid");
                break;
            case "Green Stripes": default:
                cardBack = new Back("images/Green_Stripes.svg", "green-stripes");
        }

        this.deck = new Deck(cardSequence, this.main, cardType, cardBack);
    },

    layOut: function() {

        // Encapsulates how cards are laid out

        var fragment = document.createDocumentFragment();

        this.deck.cards.forEach(function(card) {
            var cardSlot = document.createElement("div");
            cardSlot.classList.add("card-slot");
            cardSlot.appendChild(card.domRepresentation);
            fragment.appendChild(cardSlot);
        });

        this.main.appendChild(fragment);

        window.dispatchEvent(new CustomEvent("cardslayedout"));

    },

    addCardToDeck: function(value) {
        var newCard = this.deck.add(value),
            cardSlot = document.createElement("div");

        cardSlot.classList.add("card-slot");
        cardSlot.appendChild(newCard.domRepresentation);
        this.main.insertBefore(cardSlot, this.main.firstChild);

        window.dispatchEvent(new CustomEvent("cardslayedout"));
    },

    deckSize: function() {
        return this.deck.cards.length;
    },

    destroyDeck: function() {
        var that = this;
        this.deck.cards.forEach(function(card) {
            that.main.removeChild(card.domRepresentation.parentNode);
        });
    }

};

poker.setup();