var overlay = {

    domRepresentation: null,

    create: function() {
        this.domRepresentation = document.createElement("div");
        this.domRepresentation.id = "overlay";
        document.querySelector("[role='main']").appendChild(this.domRepresentation);
    },

    show: function() {
        this.domRepresentation.classList.add("show");
        this.domRepresentation.classList.remove("hide");
    },

    hide: function() {
        this.domRepresentation.classList.remove("show");
        this.domRepresentation.classList.add("hide");
    }

};