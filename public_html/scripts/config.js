var config = {};

Object.defineProperty(config, "deck", {
    get: function() {
        var storedValue = window.localStorage.getItem("deck");

        // Default value
        if (storedValue === null) {
            return "Fibonacci";
        }

        return  storedValue;
    },
    set: function(deck) {
        window.localStorage.setItem("deck", deck);
    }
});

Object.defineProperty(config, "flip", {
    get: function() {
        var storedValue = window.localStorage.getItem("flip");

        // Default value
        if (storedValue === null) {
            return true;
        }

        return storedValue === "true";
    },
    set: function(flip) {
        window.localStorage.setItem("flip", flip);
    }
});

Object.defineProperty(config, "communicationCards", {
    get: function() {
        var storedValue = window.localStorage.getItem("communication.cards");

        // Default value
        if (storedValue === null) {
            return false;
        }

        return storedValue === "true";
    },
    set: function(communicationCards) {
        window.localStorage.setItem("communication.cards", communicationCards);
    }
});

Object.defineProperty(config, "image", {
    get: function() {
        var storedValue = window.localStorage.getItem("image");

        // Default value
        if (storedValue === null) {
            return "Green Stripes";
        }

        return storedValue;
    },
    set: function(image) {
        window.localStorage.setItem("image", image);
    }
});