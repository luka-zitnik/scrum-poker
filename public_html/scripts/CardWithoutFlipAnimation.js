var CardWithoutFlipAnimation = function(value, table, back) {
    Card.call(this, value, table, back);
};

CardWithoutFlipAnimation.prototype = Object.create(Card.prototype, {
    poker: {
        value: function() {
            if (this.isRevealed()) {
                this.returnToOriginalState();
            }
            else {
                this.select();
                this.reveal();
            }
        }
    }
});

CardWithoutFlipAnimation.prototype.constructor = Card;