var Card = function(value, table, back) {

    this.value = value;
    this.table = table;

    this.domRepresentation = document.createElement("div");
    this.domRepresentation.classList.add("card");

    var frontSide = document.createElement("figure");
    frontSide.classList.add("front");
    this.domRepresentation.appendChild(frontSide);

    var caption = document.createElement("span");
    caption.textContent = value;
    function resizeFont() {
        caption.style.fontSize = (window.innerWidth * [, 2.4, 2.1, 1.5][value.length]) + "%";
    }
    window.addEventListener("resize", resizeFont);
    resizeFont();
    frontSide.appendChild(caption);

    var backSide = document.createElement("figure");
    backSide.classList.add("back");

    var object = document.createElement("object");
    object.type = "image/svg+xml";
    object.data = back.path;
    object.classList.add(back.class);
    backSide.appendChild(object);

    this.domRepresentation.appendChild(backSide);

    // Calculate offset once -- prevents randomness of its values during animations
    var recalculatePosition = function() {
        this.offsetLeft = calculatePosition(this.domRepresentation, "offsetLeft");
        this.offsetTop = calculatePosition(this.domRepresentation, "offsetTop");
    }.bind(this);
    window.addEventListener("cardslayedout", recalculatePosition);
    window.addEventListener("resize", recalculatePosition);

    this.domRepresentation.addEventListener("click", this.poker.bind(this));

    // Define inside constructor in order to bind this to Card instance
    this.shakeToReveal = function() {
        this.reveal();
        navigator.vibrate(100);
    }.bind(this);

};

Card.prototype.select = function() {
    this.notifyStart();

    this.centerOnViewport();
    this.domRepresentation.classList.add("flipped");
    window.addEventListener("shake", this.shakeToReveal, false);
};

Card.prototype.reveal = function() {
    this.domRepresentation.classList.add("revealed");
    window.removeEventListener("shake", this.shakeToReveal, false);
};

Card.prototype.isSelected = function() {
    return this.domRepresentation.classList.contains("flipped");
};

Card.prototype.isRevealed = function() {
    return this.domRepresentation.classList.contains("revealed");
};

Card.prototype.centerOnViewport = function() {
    var left = (
            this.table.scrollLeft
            - this.offsetLeft
            + document.documentElement.clientWidth / 2
            - this.domRepresentation.offsetWidth / 2
            ) + "px",
        top = (
            this.table.scrollTop
            - this.offsetTop
            + document.documentElement.clientHeight / 2
            - this.domRepresentation.offsetHeight / 2
            ) + "px",
        style = this.domRepresentation.style;

    style.setProperty("left", left);
    style.setProperty("top", top);
};

Card.prototype.returnToOriginalState = function() {
    var style = this.domRepresentation.style,
        classList = this.domRepresentation.classList;

    this.notifyEnd();

    style.removeProperty("left");
    style.removeProperty("top");
    classList.remove("revealed");
    classList.remove("flipped");
};

Card.prototype.poker = function() {
    if (this.isRevealed()) {
        this.returnToOriginalState();
    }
    else if (this.isSelected()) {
        this.reveal();
    }
    else {
        this.select();
    }
};

Card.prototype.notifyStart = function() {
    window.dispatchEvent(new CustomEvent("pokerstart"));
};

Card.prototype.notifyEnd = function() {
    window.dispatchEvent(new CustomEvent("pokerend"));
};