// Iterates through all of an element's parent nodes to give a final value.
// The function takes two parameters; objElement (the name of the element in
// question), and the offset property (offsetLeft or offsetTop).
// Source, http://www.w3.org/TR/WCAG20-TECHS/SCR34.html
function calculatePosition(objElement, strOffset)
{
    var iOffset = 0;

    if (objElement.offsetParent)
    {
        do
        {
            iOffset += objElement[strOffset];
            objElement = objElement.offsetParent;
        } while (objElement);
    }

    return iOffset;
}